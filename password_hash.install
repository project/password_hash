<?php

function password_hash_enable() {
  $t = get_t();
  if (!defined('DRUPAL_ROOT')) {
    drupal_set_message($t('Could not override the password hash implementation. Please disable and enable this module again.'), 'error');
    return;
  }

  $path = __DIR__;
  $path = substr($path, strlen(DRUPAL_ROOT));
  $path = trim($path, '\\/');
  $path = str_replace('\\', '/', $path);

  if (file_exists(DRUPAL_ROOT . '/' . $path . '/' . 'password_hash.password.inc')) {
    $path = $path . '/' . 'password_hash.password.inc';
    variable_set('password_inc', $path);
    $t('Password hashing implementation is now overridden. Existing passwords will continue to work, and they will be replaced with the newer implementation on their first successful login or reset.');
  }
}

function password_hash_disable() {
  variable_set('password_inc', 'includes/password.inc');
  $t = get_t();
  drupal_set_message($t('Password hashing implementation has been reverted to Drupal core\'s. Existing 
  passwords that were converted by this module will no longer work, and a password
  reset might be necessary.'), 'warning');
}

/**
 * Implements hook_requirements().
 *
 * @param $phase
 * @return mixed
 */
function password_hash_requirements($phase) {
  if ($phase == 'runtime') {
    $t = get_t();
    $value = '';
    $severity = REQUIREMENT_INFO;

    if (!function_exists('password_hash')) {
      $value = $t('Your system does not support password_hash() function. Please make sure that you PHP version is 5.5 or later, and supports password hashing. If you cannot make changes, you should <strong>immediately</strong> uninstall this module.');
      $severity = REQUIREMENT_ERROR;
    }

    if ($file = variable_get('password_inc', '')) {
      if (basename($file) !== 'password_hash.password.inc') {
        $value = $t('Your system supports password_hash() function, but your site does not use it. Please reinstall the Password Hash module to fix this.');
        $severity = REQUIREMENT_ERROR;
      }
      else {
        require_once DRUPAL_ROOT . '/' . $file;
        $hash = user_hash_password('Drupal');

        if ($hash) {
          $hash = password_get_info($hash);

          $info = array();

          $info[] = $t('Passwords are being hashed with %algorithm.', array(
            '%algorithm' => $hash['algoName'],
          ));

          $options = array();
          if (!empty($hash['options']) && is_array($hash['options'])) {
            foreach ($hash['options'] as $option => $value) {
              $options[] .= $option . '=' . $value;
            }
          }

          if ($options) {
            $info[] = $t('Options: %options.', array('%options' => implode(',', $options)));
          }

          $severity = REQUIREMENT_OK;
          if ($hash['algoName'] !== 'argon2i') {
            if (defined('PASSWORD_ARGON2I')) {
              $info[] = $t('Your system supports Argon2i password hashing algorithm. You can optionally opt to use it. <a href="@documentation-url">Refer the documentation</a> to enable it.', array(
                '@documentation-url' => 'https://www.drupal.org/docs/7/modules/php-native-password_hash-for-drupal/introduction-installation-and-configuration#argon2i',
              ));
            }
            else {
              $info[] = $t('Your system does not support Argon2i password hashing algorithm (requires PHP 7.2). This does not necessarily indicate any security issue.');
            }
          }

          $value = implode(' ', $info);
        }
      }
    }

    if ($value) {
      $requirements['password_hash'] = array(
        'title' => $t('Password hashing'),
        'value' => $value,
        'severity' => $severity,
      );

      return $requirements;
    }
  }
}


/**
 * Adding support for Argon2i password hashing algorithm (no changes will be made this until explicitly enabled).
 */
function password_hash_update_7101() {
  $t = get_t();
  drupal_set_message($t('This version of the "PHP Native password hash" module now supports Argon2i password hashing algorithm. <a href="@documentation-url">Refer the documentation</a> to enable support.', array('@url' => 'https://www.drupal.org/docs/7/modules/php-native-password_hash-for-drupal/introduction-installation-and-configuration#argon2i')));
}
