**PHP Native password_hash for Drupal**

This module can seamlessly swap Drupal core's password hashing algorithm with password_hash() function provided by recent PHP versions.

*Requires PHP 5.5 or later.*

**How it works**

When Drupal needs to verify the user password, it has the password user just-entered, and the hashed password of that user. To validate the password, system needs to identify the type of the password hash. This is done by analyzing the hash itself. This hash could be from an older Drupal version, from the standard Drupal core hashing, or one of the algorithms provided by this module.

Once the algorithm is figured out, the user-entered password is hashed with the same hashing algorithm and options, and the hash is compared securely (time-safe comparison) to the password hash stored in the database.

If the hashes match (which means user entered the correct password), system checks if it is necessary to rehash the stored password. If it is necessary, the hash stored in the database will be updated as well.

**Installation**

To install, please follow [this highly detailed guide](https://www.drupal.org/docs/7/extending-drupal-7/installing-contributed-modules).

**Configuration**

Module works out-of-the-box from the time you install it. No configuration is required. Upon installation, module overrides the Drupal's password hashing subsystem with the new one provided by this module.

**Advanced configuration**

Alright, you need to be careful at this part. For those who are not satisfied with the sane defaults provided by the module and your PHP configuration, you can override the password hashing algorithm and its options where applicable.

The configuration is set from your sites `settings.php` file. Unless you have a multi-site setup, or a Drupal-optimized hosting platform that overrides the file name, this file is `sites/default/settings.php`. You might need to change permissions in order to edit this file.

*Use Argon2i hashing algorithm:*
 To use `Argon2i` algorithm, put the following at the end of your `settings.php` file:

    $conf['password_hash_algo_name'] = PASSWORD_ARGON2I;

Note that the above code will throw an error on PHP versions below `7.2`.

*Override hashing options:*
  You can also override the hashing options from a configuration directive like above. Please note that the settings are not validated by the module, and misconfiguring these options can make the Winter come sooner.

If you are using Bcrypt, you can override the cost factor like this:

    $conf['password_hash_algo_options'] = array(
      'cost' => 12,
    );
 For Argon2i, the following configuration options can be used:

     $conf['password_hash_algo_options'] = array(
       'memory_cost' => 1024,
       'time_cost' => 2,
       'threads' => 2,
     );
Unless you know what you are doing, we highly discourage you from changing these values. Leaving default values means newer PHP versions can adjust these parameters automatically.
